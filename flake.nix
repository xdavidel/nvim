{
  description = "Setup LazyVim using NixVim";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    flake-parts.inputs.nixpkgs-lib.follows = "nixpkgs";

    arachne = {
      url = "github:xdavidel/arachne.nvim";
      flake = false;
    };

    telescope-heading = {
      url = "github:crispgm/telescope-heading.nvim";
      flake = false;
    };

    bookmarks = {
      url = "github:tomasky/bookmarks.nvim";
      flake = false;
    };

    hi-my-words = {
      url = "github:dvoytik/hi-my-words.nvim";
      flake = false;
    };

    cscope_maps = {
      url = "github:dhananjaylatkar/cscope_maps.nvim";
      flake = false;
    };

    silicon = {
      url = "github:michaelrommel/nvim-silicon";
      flake = false;
    };

    git-worktree = {
      url = "github:polarmutex/git-worktree.nvim";
      flake = false;
    };

    lspmark = {
      url = "github:xdavidel/lspmark.nvim";
      flake = false;
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-parts,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];

      perSystem = {
        pkgs,
        lib,
        system,
        ...
      }: let
        # Derivation containing all plugins
        pluginPath = import ./nix/plugins.nix {inherit pkgs lib inputs;};

        # Derivation containing all runtime dependencies
        runtimePath = import ./nix/runtime.nix {inherit pkgs;};

        # Link together all treesitter grammars into single derivation
        treesitterPath = pkgs.symlinkJoin {
          name = "lazyvim-nix-treesitter-parsers";
          paths = pkgs.vimPlugins.nvim-treesitter.withAllGrammars.dependencies;
        };

        # Wrap neovim with custom init and plugins
        neovimWrapped = pkgs.wrapNeovim pkgs.neovim-unwrapped {
          extraMakeWrapperArgs = "--suffix PATH : ${runtimePath}/bin";
          viAlias = true;
          vimAlias = true;
          withNodeJs = false;
          withRuby = false;
          withPython3 = true;
          configure = {
            customRC =
              /*
              vim
              */
              ''
                " Populate paths to neovim
                let g:config_path = "${./.}"
                let g:plugin_path = "${pluginPath}"
                let g:runtime_path = "${runtimePath}"
                let g:treesitter_path = "${treesitterPath}"
                " Begin initialization
                source ${./nixinit.lua}
              '';
            packages.all.start = [pkgs.vimPlugins.lazy-nvim];
          };
        };
      in {
        formatter = nixpkgs.legacyPackages.${system}.alejandra;
        packages.default = neovimWrapped;
      };
    };
}
