{pkgs, ...}: let
  cscope_lsp = import ./cscope_lsp.nix {inherit pkgs;};

  # codelldb executable is not exported by default
  lazy-codelldb = pkgs.writeShellScriptBin "codelldb" ''
    nix shell --impure --expr 'with import (builtins.getFlake "nixpkgs") {}; writeShellScriptBin "codelldb" "''${pkgs.vscode-extensions.vadimcn.vscode-lldb}/share/vscode/extensions/vadimcn.vscode-lldb/adapter/codelldb $@"' --command codelldb "$@"
  '';

  # cmake-lint is used as cmakelint
  lazy-cmakelint = pkgs.writeShellScriptBin "cmakelint" ''
    nix shell nixpkgs#cmake-format --command cmake-lint "$@"
  '';

  lazy-clangd = pkgs.writeShellScriptBin "clangd" ''
    if [ -f /opt/vector-clang-tidy/bin/clangd ]; then
      /opt/vector-clang-tidy/bin/clangd "$@"
    else
      nix shell nixpkgs#clang-tools_16 --command clangd "$@"
    fi
  '';

  make-lazy = pkg: bin:
    pkgs.writeShellScriptBin "${bin}" ''
      nix shell nixpkgs#${pkg} --command ${bin} "$@"
    '';

  wrap-bin = pkg: bin:
    pkgs.writeShellScriptBin "${bin}" ''
      exec ${pkgs.${pkg}}/bin/${bin} "$@"
    '';
in
  # Link together all runtime dependencies into one derivation
  pkgs.symlinkJoin {
    name = "nvim-nix-runtime";
    paths = with pkgs; [
      # LazyVim dependencies
      lazygit
      ripgrep
      fd
      git # for status

      cscope_lsp
      cscope

      silicon

      # LSP's
      (wrap-bin "clang-tools_16" "clangd")
      nil
      (wrap-bin "pyright" "pyright-langserver")
      (wrap-bin "rust-analyzer" "rust-analyzer")
      (wrap-bin "zls" "zls")
      (wrap-bin "marksman" "marksman")
      (wrap-bin "yaml-language-server" "yaml-language-server")
      lua-language-server

      # Debuggers
      # codelldb

      # Formatters
      (wrap-bin "stylua" "stylua") # lua
      (wrap-bin "jq" "jq") # json
      alejandra # nix
      isort # python (sorting imports)
      black # python
      rustfmt

      # Linters
      (wrap-bin "cmake-format" "cmake-format")

      # Bundle also cmake
      (wrap-bin "cmake" "cmake")
    ];
  }
