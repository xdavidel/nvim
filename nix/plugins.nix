{
  pkgs,
  inputs,
  lib,
  ...
}: let
  mkEntryFromDrv = drv:
    if lib.isDerivation drv
    then {
      name = "${lib.getName drv}";
      path = drv;
    }
    else drv;

  arachne = pkgs.vimUtils.buildVimPlugin {
    name = "arachne";
    src = inputs.arachne;
  };
  telescope-heading = pkgs.vimUtils.buildVimPlugin {
    name = "telescope-heading";
    src = inputs.telescope-heading;
  };
  bookmarks = pkgs.vimUtils.buildVimPlugin {
    name = "bookmarks";
    src = inputs.bookmarks;
  };
  hi-my-words = pkgs.vimUtils.buildVimPlugin {
    name = "hi-my-words";
    src = inputs.hi-my-words;
  };

  cscope_maps = pkgs.vimUtils.buildVimPlugin {
    name = "cscope_maps";
    src = inputs.cscope_maps;
  };

  silicon = pkgs.vimUtils.buildVimPlugin {
    name = "nvim-silicon";
    src = inputs.silicon;
  };

  git-worktree = pkgs.vimUtils.buildVimPlugin {
    name = "git-worktree";
    src = inputs.git-worktree;
  };

  lspmark = pkgs.vimUtils.buildVimPlugin {
    name = "lspmark";
    src = inputs.lspmark;
  };

  plugins = with pkgs.vimPlugins; [
    {
      name = "arachne.nvim";
      path = arachne;
    }
    {
      name = "telescope-heading.nvim";
      path = telescope-heading;
    }
    {
      name = "bookmarks.nvim";
      path = bookmarks;
    }
    {
      name = "catppuccin";
      path = catppuccin-nvim;
    }
    {
      name = "LuaSnip";
      path = luasnip;
    }
    {
      name = "dropbar.nvim";
      path = dropbar-nvim;
    }
    {
      name = "todo-comments.nvim";
      path = todo-comments-nvim;
    }
    {
      name = "hi-my-words.nvim";
      path = hi-my-words;
    }
    {
      name = "cscope_maps.nvim";
      path = cscope_maps.overrideAttrs {
        nvimSkipModule = [
          "cscope.pickers.telescope"
          "cscope.pickers.fzf-lua"
        ];
      };
    }
    {
      name = "render-markdown.nvim";
      path = render-markdown-nvim;
    }

    {
      name = "nvim-silicon";
      path = silicon;
    }
    {
      name = "git-worktree.nvim";
      path = git-worktree.overrideAttrs {
        nvimSkipModule = [
          # vim plugin with optional toggleterm integration
          "git-worktree"
          "git-worktree.worktree"
          "git-worktree.git"
          "git-worktree.hooks"
        ];
      };
    }
    {
      name = "lspmark.nvim";
      path = lspmark;
    }

    bigfile-nvim
    diffview-nvim
    flash-nvim
    gitsigns-nvim
    lualine-nvim
    mkdir-nvim
    neogit
    vim-cool
    nvim-colorizer-lua
    nvim-treesitter
    nvim-treesitter-textobjects
    nvim-ts-context-commentstring
    oil-nvim
    surround-nvim
    telescope-nvim
    telescope-fzf-native-nvim
    telescope-ui-select-nvim
    toggleterm-nvim
    venn-nvim
    which-key-nvim

    neo-tree-nvim
    noice-nvim

    orgmode

    nvim-navic

    # cmp
    blink-cmp
    blink-compat
    blink-emoji-nvim
    cmp-calc
    friendly-snippets

    # lsp
    lsp_lines-nvim
    nvim-lspconfig
    lazydev-nvim
    conform-nvim

    sniprun
    overseer-nvim

    # deps
    nvim-web-devicons
    plenary-nvim
    nui-nvim
  ];
in
  # Link together all plugins into a single derivation
  pkgs.linkFarm "nvim-nix-plugins" (builtins.map mkEntryFromDrv plugins)
