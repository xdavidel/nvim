{pkgs}: let
  binary = "cscope_lsp";
in
  pkgs.stdenv.mkDerivation {
    name = "${binary}";
    src = pkgs.fetchFromGitHub {
      owner = "dhananjaylatkar";
      repo = "cscope_lsp";
      rev = "c9ff314dd479ffe07dc3d1fd6fc1762bbb973b14";
      sha256 = "9jMu6xN0/tH9GI5XTC2lVJTjlZ7Ghg/PtXI7/wbyY40=";
    };

    nativeBuildInputs = [
      pkgs.go
    ];

    buildPhase = ''
      export HOME=$(pwd) # fix homeless-shelter error
      go build
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp ${binary} $out/bin
    '';
  }
