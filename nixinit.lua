-- Some nix setups
vim.g.nixenv = true
vim.opt.rtp:prepend(vim.g.config_path)

require("config")

require("lazy").setup({
	defaults = { lazy = true },
	change_detection = {
		-- automatically check for config file changes and reload the ui
		enabled = false,
		notify = false, -- get a notification when changes are found
	},
	dev = {
		-- reuse files from pkgs.vimPlugins.*
		path = vim.g.plugin_path,
		patterns = { "." },
		-- fallback to download
		fallback = false,
	},
	spec = {
		{ import = "plugins" },
		-- -- put this line at the end of spec to clear ensure_installed
		{
			"nvim-treesitter/nvim-treesitter",
			init = function()
				-- Put treesitter path as first entry in rtp
				vim.opt.rtp:prepend(vim.g.treesitter_path)
			end,
			opts = { auto_install = false, ensure_installed = {} },
		},
	},
	performance = {
		rtp = {
			-- Setup correct config path
			paths = { vim.g.config_path, vim.g.config_path .. "/after" },
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"netrwPlugin",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})

-- remove "normal" paths
vim.opt.rtp:remove(vim.fn.stdpath("config"))
vim.opt.rtp:remove(vim.fn.stdpath("config") .. "/after")
vim.opt.rtp:remove(vim.fn.stdpath("data") .. "/site")
vim.opt.rtp:remove(vim.fn.stdpath("data") .. "/site/after")
