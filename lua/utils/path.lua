local M = {}

if vim.loop.os_uname().version:match("Windows") then
	M.sep = "\\"
else
	M.sep = "/"
end

M.join = function(...)
	local result = table.concat({ ... }, M.sep)
	return result
end

local is_inside_work_tree = {}
M.is_git_repo = function()
	local cwd = vim.fn.getcwd()
	if is_inside_work_tree[cwd] == nil then
		vim.fn.system("git rev-parse --is-inside-work-tree")
		is_inside_work_tree[cwd] = vim.v.shell_error == 0
	end

	return is_inside_work_tree[cwd]
end

M.get_git_root = function()
	local dot_git_path = vim.fn.finddir(".git", ".;")
	return vim.fn.fnamemodify(dot_git_path, ":h")
end

return M
