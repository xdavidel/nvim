local M = {}
--- A modified version of telescope `live_grep`
--- - Notes:
---   - Allows passing files to glob using double spaces
local live_filegrep = function(opts)
	local actions = require("telescope.actions")
	local finders = require("telescope.finders")
	local make_entry = require("telescope.make_entry")
	local pickers = require("telescope.pickers")
	local sorters = require("telescope.sorters")
	local utils = require("telescope.utils")
	local conf = require("telescope.config").values

	local flatten = utils.flatten

	opts = opts or {}

	opts.picker_name = "Live FileGrep"

	local vimgrep_arguments = opts.vimgrep_arguments or conf.vimgrep_arguments
	if not vim.fn.executable(vimgrep_arguments[1]) == 1 then
		local picker_name = opts.picker_name
		utils.notify(picker_name, {
			msg = string.format(
				"'ripgrep', or similar alternative, is a required dependency for the %s picker. ",
				picker_name
			),
			level = "ERROR",
		})
		return
	end

	local search_dirs = opts.search_dirs
	opts.cwd = opts.cwd and utils.path_expand(opts.cwd) or vim.loop.cwd()

	if search_dirs then
		for i, path in ipairs(search_dirs) do
			search_dirs[i] = utils.path_expand(path)
		end
	end

	local additional_args = {}
	if opts.additional_args ~= nil then
		if type(opts.additional_args) == "function" then
			additional_args = opts.additional_args(opts)
		elseif type(opts.additional_args) == "table" then
			additional_args = opts.additional_args
		end
	end

	if opts.type_filter then
		additional_args[#additional_args + 1] = "--type=" .. opts.type_filter
	end

	if type(opts.glob_pattern) == "string" then
		additional_args[#additional_args + 1] = "--glob=" .. opts.glob_pattern
	elseif type(opts.glob_pattern) == "table" then
		for i = 1, #opts.glob_pattern do
			additional_args[#additional_args + 1] = "--glob=" .. opts.glob_pattern[i]
		end
	end

	if opts.file_encoding then
		additional_args[#additional_args + 1] = "--encoding=" .. opts.file_encoding
	end

	local args = flatten({ vimgrep_arguments, additional_args })

	local live_filegrepper = finders.new_job(function(prompt)
		if not prompt or prompt == "" then
			return nil
		end

		local globs = {}
		local parts = vim.split(prompt, "  ")
		if parts[1] then
			prompt = parts[1]
		end

		for i, glob in ipairs(parts) do
			if i > 1 then
				globs[#globs + 1] = "--glob=" .. glob
			end
		end

		local search_list = {}

		if search_dirs then
			search_list = search_dirs
		end

		local retval = flatten({ args, globs, "--", prompt, search_list })
		return retval
	end, opts.entry_maker or make_entry.gen_from_vimgrep(opts), opts.max_results, opts.cwd)

	pickers
		.new(opts, {
			prompt_title = opts.picker_name,
			finder = live_filegrepper,
			previewer = conf.grep_previewer(opts),
			sorter = sorters.highlighter_only(opts),
			attach_mappings = function(_, map)
				map("i", "<c-space>", actions.to_fuzzy_refine)
				return true
			end,
			push_cursor_on_edit = true,
		})
		:find()
end

M.find_project_files = function(opts)
	opts = opts or {}
	if require("utils.path").is_git_repo() then
		opts.cwd = require("utils.path").get_git_root()
	end
	require("telescope.builtin").find_files(opts)
end

M.find_project_text = function(opts)
	opts = opts or {}
	if require("utils.path").is_git_repo() then
		opts.cwd = require("utils.path").get_git_root()
	end

	live_filegrep(opts)
end

M.find_project_current_string = function(opts)
	opts = opts or {}
	if require("utils.path").is_git_repo() then
		opts.cwd = require("utils.path").get_git_root()
	end

	require("telescope.builtin").grep_string(opts)
end

return M
