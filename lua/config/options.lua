local tab_spaces = 4

local setting = {
	g = {
		mapleader = " ",
		maplocalleader = " ",
	},
	opt = {
		-- Use UTF-8
		fileencoding = "utf-8",

		-- Clipboard On
		clipboard = "unnamedplus",

		-- Incremental live completion
		inccommand = "nosplit",

		-- Set line numbers default
		number = true,
		relativenumber = false,

		-- Do not save when switching buffers
		hidden = true,

		-- Enable mouse mode
		mouse = "a",

		-- Enable wrap lines indentation
		breakindent = true,

		-- Case insensitive searching UNLESS /C or capital in search
		ignorecase = true,
		smartcase = true,

		-- Tabs
		expandtab = true,
		tabstop = tab_spaces,
		softtabstop = tab_spaces,
		shiftwidth = tab_spaces,

		-- Normal Splits
		splitbelow = true,
		splitright = true,

		-- No Wrapping
		wrap = false,

		-- Backups and swap
		swapfile = false,
		backup = false,
		undodir = vim.fn.stdpath("cache") .. "/undo",
		undofile = true, -- enable persistent undo
		writebackup = false, -- don't allow editing open files

		-- Better color support
		termguicolors = vim.fn.has("termguicolors") == 1,

		-- Show signs in the number column
		signcolumn = "yes",

		-- always show completion box and don't preselect
		completeopt = "menuone,noselect",

		-- Limit max completion items
		pumheight = 10,

		-- Decrease update time
		updatetime = 250,

		cursorline = true,

		numberwidth = 4,

		-- start scrolling offset
		scrolloff = 5,

		list = false,

		filetype = "plugin",

		-- Folds
		foldmethod = "manual",

		smartindent = true,

		iskeyword = "@,48-57,_,192-255",

		laststatus = 3,

		linebreak = true,

		fillchars = "vert:┃,horiz:━,verthoriz:╋,horizup:┻,horizdown:┳,vertleft:┫,vertright:┣,eob: ",
	},
}

for prefix, tab in pairs(setting) do
	for key, value in pairs(tab) do
		vim[prefix][key] = value
	end
end

vim.opt.listchars:append("space:⋅")
vim.opt.listchars:append("tab:⭲ ")
-- vim.opt.listchars:append("tab:↹ ")
vim.opt.listchars:append("eol:↴")

vim.keymap.set("n", "<leader>tL", function()
	vim.opt.list = not vim.opt.list:get()
	vim.notify(string.format("Listchars %s", vim.opt.list:get()))
end, { desc = "[T]oggle [L]ist chars" })

vim.opt.shortmess:append("sI")

return setting
