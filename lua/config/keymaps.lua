local function register(key_map)
	key_map.options = key_map.options and key_map.options or {}
	key_map.options.desc = key_map.description
	vim.keymap.set(key_map.mode, key_map.lhs, key_map.rhs, key_map.options)
end

-- bulk registration shortcuts
local function bulk_register(group_keymap, opts)
	for _, key_map in pairs(group_keymap) do
		if opts then
			key_map.options = opts
		end
		register(key_map)
	end
end

bulk_register({
	{
		mode = { "n" },
		lhs = "<Space>",
		rhs = "<Nop>",
		options = { noremap = true, silent = true },
		description = "Leader key",
	},
	{
		mode = { "n" },
		lhs = "k",
		rhs = "v:count == 0 ? 'gk' : 'k'",
		options = { noremap = true, expr = true, silent = true },
		description = "Up row even in wrap mode",
	},
	{
		mode = { "n" },
		lhs = "j",
		rhs = "v:count == 0 ? 'gj' : 'j'",
		options = { noremap = true, expr = true, silent = true },
		description = "Down row even in wrap mode",
	},
	{
		mode = { "n" },
		lhs = "<M-z>",
		rhs = ":set wrap!<cr>",
		options = { noremap = true, silent = true },
		description = "Toggle wrap mode",
	},
	{
		mode = { "i" },
		lhs = "<M-z>",
		rhs = "<Esc>:set wrap!<cr>a",
		options = { noremap = true, silent = true },
		description = "Toggle wrap mode",
	},
	{
		mode = { "n" },
		lhs = "<C-A-Up>",
		rhs = ":resize -2<CR>",
		options = { silent = true },
		description = "Resize window up",
	},
	{
		mode = { "n" },
		lhs = "<C-A-Down>",
		rhs = ":resize +2<CR>",
		options = { silent = true },
		description = "Resize window down",
	},
	{
		mode = { "n" },
		lhs = "<C-A-Left>",
		rhs = ":vertical resize -2<CR>",
		options = { silent = true },
		description = "Resize window left",
	},
	{
		mode = { "n" },
		lhs = "<C-A-Right>",
		rhs = ":vertical resize +2<CR>",
		options = { silent = true },
		description = "Resize window right",
	},
	{
		mode = { "t" },
		lhs = "<Esc>",
		rhs = [[<C-\><C-n>]],
		options = { silent = true },
		description = "Escape insert mode in terminal",
	},
	{
		mode = { "t" },
		lhs = "<Esc><Esc>",
		rhs = "<Esc>",
		options = { silent = true },
		description = "Nornal escape in terminal",
	},
	{
		mode = { "n" },
		lhs = "Y",
		rhs = "y$",
		options = { noremap = true },
		description = "Yank to the end of the line",
	},
	{
		mode = { "v" },
		lhs = "<",
		rhs = "<gv",
		options = { noremap = true, silent = true },
		description = "Indent left",
	},
	{
		mode = { "v" },
		lhs = ">",
		rhs = ">gv",
		options = { noremap = true, silent = true },
		description = "Indent right",
	},
	{
		mode = { "x" },
		lhs = "*",
		rhs = 'y/\\V<C-R>"<CR>',
		options = { noremap = true, silent = true },
		description = "Search forward",
	},
	{
		mode = { "x" },
		lhs = "#",
		rhs = 'y?\\V<C-R>"<CR>',
		options = { noremap = true, silent = true },
		description = "Search backward",
	},
	{
		mode = { "n" },
		lhs = "tn",
		rhs = ":tabnext<CR>",
		options = { noremap = true, silent = true },
		description = "Next tab",
	},
	{
		mode = { "n" },
		lhs = "tp",
		rhs = ":tabprevious<CR>",
		options = { noremap = true, silent = true },
		description = "Prev tab",
	},
	{
		mode = { "n" },
		lhs = "tx",
		rhs = ":tabclose<CR>",
		options = { noremap = true, silent = true },
		description = "Close tab",
	},
	{
		mode = { "n" },
		lhs = "tc",
		rhs = ":tabnew<CR>",
		options = { noremap = true, silent = true },
		description = "New tab",
	},
	{
		mode = { "n" },
		lhs = "<A-Down>",
		rhs = ":m .+1<CR>==",
		options = { noremap = true, silent = true },
		description = "Move line down",
	},
	{
		mode = { "n" },
		lhs = "<A-Up>",
		rhs = ":m .-2<CR>==",
		options = { noremap = true, silent = true },
		description = "Move line up",
	},
	{
		mode = { "i" },
		lhs = "<A-Down>",
		rhs = "<Esc>:m .+1<CR>==gi",
		options = { noremap = true, silent = true },
		description = "Move line down",
	},
	{
		mode = { "i" },
		lhs = "<A-Up>",
		rhs = "<Esc>:m .-2<CR>==gi",
		options = { noremap = true, silent = true },
		description = "Move line up",
	},
	{
		mode = { "x" },
		lhs = "<A-Down>",
		rhs = ":m '>+1<CR>gv-gv",
		options = { noremap = true, silent = true },
		description = "Move line down",
	},
	{
		mode = { "x" },
		lhs = "<A-Up>",
		rhs = ":m '<-2<CR>gv-gv",
		options = { noremap = true, silent = true },
		description = "Move line up",
	},
	{
		mode = { "n" },
		lhs = "]q",
		rhs = ":cnext<CR>",
		options = { noremap = true, silent = true },
		description = "Next quickfix",
	},
	{
		mode = { "n" },
		lhs = "[q",
		rhs = ":cprev<CR>",
		options = { noremap = true, silent = true },
		description = "Prev quickfix",
	},
	{
		mode = { "n" },
		lhs = "n",
		rhs = "nzzzv",
		options = { noremap = true, silent = true },
		description = "Next search (center cursor)",
	},
	{
		mode = { "n" },
		lhs = "N",
		rhs = "Nzzzv",
		options = { noremap = true, silent = true },
		description = "Prev search (center cursor)",
	},
	{
		mode = { "n" },
		lhs = "dc",
		rhs = "dt_",
		options = { noremap = true, silent = true },
		description = "Delete until _",
	},
	{
		mode = { "n" },
		lhs = "cd",
		rhs = "ct_",
		options = { noremap = true, silent = true },
		description = "Change until _",
	},
	{
		mode = { "n" },
		lhs = "/",
		rhs = "ms/",
		options = { noremap = true, silent = true },
		description = "Search forward (save position)",
	},
	{
		mode = { "n" },
		lhs = "?",
		rhs = "ms?",
		options = { noremap = true, silent = true },
		description = "Search backward (save position)",
	},
	{
		mode = { "n" },
		lhs = "dm",
		rhs = ":%s/<c-r>///g<CR>",
		options = { noremap = true, silent = true },
		description = "Delete matches",
	},
	{
		mode = { "n" },
		lhs = "cm",
		rhs = ":%s/<c-r>///g<Left><Left>",
		options = { noremap = true, silent = true },
		description = "Change matches",
	},
	{
		mode = { "n" },
		lhs = "S",
		rhs = ":%s//g<Left><Left>",
		options = { noremap = true, silent = true },
		description = "Replace all",
	},
	{
		mode = { "n", "x", "v" },
		lhs = "<leader>p",
		rhs = [["0p]],
		options = { noremap = true, silent = true },
		description = "[P]aste",
	},
	{
		mode = { "n" },
		lhs = "<leader>tt",
		rhs = function()
			vim.g.transparent_bg = not vim.g.transparent_bg
			vim.cmd(string.format("colorscheme %s", vim.g.colors_name))
		end,
		options = { noremap = true, silent = true },
		description = "[T]oggle [T]ranspancy",
	},
	{
		mode = { "n" },
		lhs = "<leader>tnn",
		rhs = function()
			vim.opt.relativenumber = false
			vim.opt.number = not vim.opt.number:get()
		end,
		options = { noremap = true, silent = true },
		description = "[T]oggle [N]umber",
	},
	{
		mode = { "n" },
		lhs = "<leader>tnr",
		rhs = function()
			vim.opt.relativenumber = not vim.opt.relativenumber:get()
		end,
		options = { noremap = true, silent = true },
		description = "[T]oggle [N]umber [R]elative",
	},
	{
		mode = { "n" },
		lhs = "<leader>ts",
		rhs = function()
			vim.opt.spell = not vim.opt.spell:get()
		end,
		options = { noremap = true, silent = true },
		description = "[T]oggle [S]pell",
	},
})
