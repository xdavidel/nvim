local define_cmds = function(cmds)
	for _, entry in ipairs(cmds) do
		local event = entry[1]
		local opts = entry[2]
		if type(opts.group) == "string" and opts.group ~= "" then
			local exists, _ = pcall(vim.api.nvim_get_autocmds, { group = opts.group })
			if not exists then
				vim.api.nvim_create_augroup(opts.group, {})
			end
		end
		vim.api.nvim_create_autocmd(event, opts)
	end
end

local cmds = {
	{
		"FileType",
		{
			group = "_close_dialogs",
			pattern = { "qf", "help", "man", "lspinfo", "null-ls-info" },
			desc = "Close those dialogs with 'q'",
			callback = function()
				vim.cmd([[
                nnoremap <silent> <buffer> q :close<CR>
                set nobuflisted
                ]])
			end,
		},
	},
	{
		"TextYankPost",
		{
			group = "_yank_highlight",
			pattern = "*",
			desc = "Highlight yanked text",
			callback = function()
				require("vim.highlight").on_yank({ higroup = "Search", timeout = 100 })
			end,
		},
	},
	{
		{ "InsertLeave", "WinEnter" },
		{

			group = "CursorLine",
			pattern = "*",
			desc = "Show cursorline on active window",
			command = "set cursorline",
		},
	},
	{
		{ "InsertEnter", "WinLeave" },
		{
			group = "CursorLine",
			pattern = "*",
			desc = "Hide cursorline on inactive window",
			command = "set nocursorline",
		},
	},
	{
		"BufReadPost",
		{
			group = "_startup",
			pattern = "*",
			desc = "Restore last cursor position in open",
			command = [[if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit' | exe "normal! g`\"" | endif]],
		},
	},
	{
		"BufWritePost",
		{
			group = "_auto_executable_file",
			pattern = "*",
			desc = "Set scripts to be executable from the shell",
			command = [[if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod +x <afile>" | endif | endif]],
		},
	},
	{
		"BufWritePre",
		{
			group = "_clean_whitespace",
			pattern = "*",
			desc = "Clean whitespaces on save",
			callback = function()
				local save = vim.fn.winsaveview()
				local patterns = {
					-- [[%s/\n\+\%$//e]], -- lastline
					[[%s/\s\+$//e]], -- trailing
				}

				for _, v in ipairs(patterns) do
					vim.cmd(string.format("keepjumps keeppatterns silent! %s", v), false)
				end
				vim.fn.winrestview(save)
			end,
		},
	},
	{
		"BufReadPost",
		{
			group = "_xresources_files",
			pattern = { ".Xresources", ".Xdefaults", "xresources", "xdefaults" },
			desc = "Open xresources files",
			command = [[set filetype=xresources]],
		},
	},
	{
		"BufNewFile",
		{
			group = "_xresources_files",
			pattern = { ".Xresources", ".Xdefaults", "xresources", "xdefaults" },
			desc = "Open xresources files",
			command = [[set filetype=xresources]],
		},
	},
}

define_cmds(cmds)

-- Auto remove background color
vim.api.nvim_create_autocmd("ColorScheme", {
	pattern = "*",
	callback = function()
		if vim.g.transparent_bg == true then
			local hl_groups = {
				"Normal",
				"NormalFloat",
				"SignColumn",
				"NormalNC",
				"TelescopeBorder",
				"NvimTreeNormal",
				"EndOfBuffer",
				"MsgArea",
			}
			for _, name in ipairs(hl_groups) do
				vim.cmd(string.format("highlight %s ctermbg=none guibg=none", name))
			end
		end
	end,
})
vim.opt.fillchars = "eob: "
