local default_diagnostic_config = {
	-- signs = {
	--   active = true,
	--   values = {
	--     { name = "DiagnosticSignError", text = "" },
	--     { name = "DiagnosticSignWarn", text = "" },
	--     { name = "DiagnosticSignHint", text = "" },
	--     { name = "DiagnosticSignInfo", text = "" },
	--   },
	-- },
	virtual_text = false,
	update_in_insert = false,
	underline = true,
	severity_sort = true,
	float = {
		focusable = true,
		style = "minimal",
		border = "rounded",
		source = "always",
		header = "",
		prefix = "",
	},
}

vim.diagnostic.config(default_diagnostic_config)
vim.keymap.set({ "n" }, "gl", "<cmd>lua vim.diagnostic.open_float()<CR>", { desc = "diagnostic" })

-- highlight extra whitespaces
vim.api.nvim_set_hl(0, "ExtraWhitespace", { bg = "Red" })
vim.fn.matchadd("ExtraWhitespace", "\\s\\+\\%#\\@<!$")
