return {
	"michaelb/sniprun",
	cmd = { "SnipInfo", "SnipRun" },
	config = function()
		require("sniprun").setup({
			display = { "TerminalWithCode" },
			interpreter_options = {
				C_original = {
					compiler = "clang",
				},
			},
		})
	end,
}
