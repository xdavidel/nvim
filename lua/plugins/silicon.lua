return {
	"michaelrommel/nvim-silicon",
	cmd = "Silicon",
	main = "nvim-silicon",
	keys = {
		{
			"<leader>tc",
			function()
				require("nvim-silicon").clip()
			end,
			desc = "[T]ake [C]code",
			mode = { "n", "v" },
		},
	},
	opts = {
		disable_defaults = false,
	},
}
