return {
	"folke/flash.nvim",
	opts = {
		modes = {
			-- a regular search with `/` or `?`
			search = {
				enabled = false,
			},
			-- `f`, `F`, `t`, `T`, `;` and `,` motions
			char = {
				enabled = false,
				-- dynamic configuration for ftFT motions
			},
		},
	},
	keys = {
		{
			"<leader><leader>",
			mode = { "n", "x", "o" },
			function()
				require("flash").jump()
			end,
			desc = "Flash Step",
		},
	},
}
