return {
	{
		"nvim-lualine/lualine.nvim",
		event = { "UIEnter" },
		dependencies = {
			{
				"nvim-tree/nvim-web-devicons",
				enabled = vim.g.nixenv == true,
			},
		},
		config = function()
			require("lualine").setup()
		end,
	},
}
