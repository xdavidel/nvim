return {
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"crispgm/telescope-heading.nvim",
		"nvim-telescope/telescope-ui-select.nvim",
		{
			"nvim-telescope/telescope-fzf-native.nvim",
			build = "make",
			cond = function()
				return vim.fn.executable("make") == 1
			end,
		},
		{
			"polarmutex/git-worktree.nvim",
			version = "^2",
			dependencies = { "nvim-lua/plenary.nvim" },
			config = function()
				local hooks = require("git-worktree.hooks")
				local config = require("git-worktree.config")
				local update_on_switch = hooks.builtins.update_current_buffer_on_switch

				hooks.register(hooks.type.SWITCH, function(path, prev_path)
					update_on_switch(path, prev_path)
				end)

				hooks.register(hooks.type.DELETE, function()
					vim.cmd(config.update_on_change_command)
				end)
			end,
		},
	},
	cmd = "Telescope",
	keys = {
		{
			"<C-p>",
			require("utils.telescope").find_project_files,
			mode = { "n", "i" },
			desc = "Find Files",
		},
		{
			"<A-o>",
			[[<cmd>Telescope heading<cr>]],
			mode = { "n", "i" },
			desc = "Find Headings",
		},
		{
			"<A-x>",
			require("telescope.builtin").commands,
			mode = { "n", "i" },
			desc = "Commands",
		},
		{
			"<C-f>",
			require("telescope.builtin").current_buffer_fuzzy_find,
			mode = { "n", "i" },
			desc = "Find",
		},
		{
			"gp",
			require("telescope.builtin").diagnostics,
			mode = { "n" },
			desc = "[G]o to [P]roblems",
		},
		{
			"<leader>sl",
			require("telescope.builtin").resume,
			mode = { "n" },
			desc = "[S]earch [L]ast",
		},
		{
			"<leader>bs",
			require("telescope.builtin").buffers,
			mode = { "n" },
			desc = "[B]uffer [S]witch",
		},
		{
			"<leader>fr",
			require("telescope.builtin").oldfiles,
			mode = { "n" },
			desc = "[F]ile [R]ecent",
		},
		{
			"<leader>gb",
			require("telescope.builtin").git_branches,
			mode = { "n" },
			desc = "[G]it [B]ranches",
		},
		{
			"<leader>gwc",
			function()
				require("telescope").extensions.git_worktree.create_git_worktree()
			end,
			mode = { "n" },
			desc = "[G]it [W]orktree [C]reate",
		},
		{
			"<leader>gws",
			function()
				-- NOTE: delete worktree using <alt-d>
				require("telescope").extensions.git_worktree.git_worktree()
			end,
			mode = { "n" },
			desc = "[G]it [W]orktree [S]witch",
		},
		{
			"<leader>sc",
			require("telescope.builtin").colorscheme,
			mode = { "n" },
			desc = "[S]earch [C]olorscheme",
		},
		{
			"<leader>sC",
			function()
				require("telescope.builtin").colorscheme({ enable_preview = true })
			end,
			mode = { "n" },
			desc = "[S]earch [C]olorscheme (preview)",
		},
		{
			"<leader>sh",
			require("telescope.builtin").help_tags,
			mode = { "n" },
			desc = "[S]earch [H]elp",
		},
		{
			"<leader>sm",
			require("telescope.builtin").marks,
			mode = { "n" },
			desc = "[S]earch [M]arks",
		},
		{
			"<leader>sM",
			require("telescope.builtin").man_pages,
			mode = { "n" },
			desc = "[S]earch [M]anpage",
		},
		{
			"<leader>sr",
			require("telescope.builtin").registers,
			mode = { "n" },
			desc = "[S]earch [R]egisters",
		},
		{
			"<leader>st",
			require("utils.telescope").find_project_text,
			mode = { "n" },
			desc = "[S]earch [T]ext",
		},
		{
			"<leader>ss",
			require("utils.telescope").find_project_current_string,
			mode = { "n" },
			desc = "[S]earch [S]elected",
		},
	},
	config = function()
		local telescope = require("telescope")
		telescope.load_extension("heading")

		pcall(telescope.load_extension, "git_worktree")

		-- Enable telescope fzf native, if installed
		pcall(telescope.load_extension, "fzf")
		pcall(telescope.load_extension, "ui-select")

		telescope.setup({
			defaults = {
				file_ignore_patterns = { "node_modules/", "cscope.*" },
			},
			extensions = {
				["ui-select"] = {
					require("telescope.themes").get_dropdown(),
				},
			},

			mappings = {
				i = {
					["<C-u>"] = false,
					["<C-d>"] = false,
				},
			},
		})
	end,
}
