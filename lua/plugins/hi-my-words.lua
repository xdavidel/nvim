return {
	"dvoytik/hi-my-words.nvim",
	keys = {
		{ "<leader>mw", "<cmd>HiMyWordsToggle<cr>", desc = "[M]ark [W]ord" },
	},
	cmd = "HiMyWordsToggle",
	config = function()
		require("hi-my-words").setup()
	end,
}
