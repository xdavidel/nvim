local symbol_picker = function(opts)
	opts = opts or { data = {} }

	local pickers = require("telescope.pickers")
	local finders = require("telescope.finders")
	local actions = require("telescope.actions")
	local action_state = require("telescope.actions.state")

	local conf = require("telescope.config").values

	pickers
		.new(opts, {
			prompt_title = "Tags",
			finder = finders.new_table({
				static = false,
				results = opts.data.keys,
				entry_maker = function(entry)
					return {
						value = entry,
						display = entry,
						ordinal = entry,
					}
				end,
			}),
			sorter = conf.generic_sorter(opts),
			attach_mappings = function(prompt_bufnr, map)
				actions.select_default:replace(function()
					actions.close(prompt_bufnr)
					local selection = action_state.get_selected_entry()
					require("cscope_maps").cscope_prompt("s", selection.value)
				end)
				return true
			end,
		})
		:find()
end

local order = function(data_str)
	local symbols = {}
	local keys = {}
	for line in string.gmatch(data_str, "([^\n]+)") do
		-- Split the line by double spaces
		local fields = {}
		for field in string.gmatch(line, "([^ ]+)  *") do
			table.insert(fields, field)
		end

		-- Check if we have at least two fields
		if #fields >= 2 then
			local key = fields[2]
			local value = fields[1]
			if not symbols[key] then
				symbols[key] = { key = key, value = value }
				table.insert(keys, key)
			else
				table.insert(symbols[key], value)
			end
		end
	end
	return { keys = keys, symbols = symbols }
end

return {
	"dhananjaylatkar/cscope_maps.nvim",
	dependencies = {
		"nvim-telescope/telescope.nvim", -- optional [for picker="telescope"]
	},
	keys = {
		{
			"<leader>cb",
			function()
				local dir = vim.fn.getcwd()
				-- local cmd = 'fd -e c -e h -e cpp -e hpp > cscope.files'
				local cmd = { "fd", "-e", "c", "-e", "h", "-e", "cpp", "-e", "hpp" }

				vim.system(cmd, { text = true, cwd = dir }, function(out)
					local filename = "cscope.files"
					local f = io.open(filename, "w")
					if not f then
						vim.notify(string.format("Could not write to: %s", filename), vim.log.levels.ERROR)
						return
					end
					f:write(out.stdout)
					f:close()
					vim.system({ "cscope", "-bqkv" }, { text = true, cwd = dir }, function(o)
						if o.code == 0 then
							vim.notify("cscope build successfully.", vim.log.levels.INFO)
						else
							vim.notify("cscope build failed!.", vim.log.levels.ERROR)
						end
					end)
				end)
			end,
			desc = "[C]scope [B]uild",
		},
		{
			"<leader>csi",
			'<cmd>exe "CsStackView open down" expand("<cword>")<cr>',
			desc = "[C]scope [S]tack [I]ngoing",
		},
		{
			"<leader>cso",
			'<cmd>exe "CsStackView open up" expand("<cword>")<cr>',
			desc = "[C]scope [S]tack [O]utgoing",
		},
		{
			"<leader>cd",
			function()
				require("cscope_maps").cscope_prompt("g", vim.fn.expand("<cword>"))
			end,
			desc = "[C]scope [D]efinitions",
		},
		{
			"<leader>cr",
			function()
				require("cscope_maps").cscope_prompt("s", vim.fn.expand("<cword>"))
			end,
			desc = "[C]scope [R]eferences",
		},
		{
			"<leader>cc",
			function()
				require("cscope_maps").cscope_prompt("d", vim.fn.expand("<cword>"))
			end,
			desc = "[C]scope [C]alls",
		},
		{
			"<leader>cv",
			function()
				require("cscope_maps").cscope_prompt("a", vim.fn.expand("<cword>"))
			end,
			desc = "[C]scope [V]alue",
		},
		{
			"<leader>ct",
			function()
				local dir = vim.fn.getcwd()
				local cmd = { "cscope", "-R", "-L", "-2", ".*" }
				vim.system(cmd, { text = true, cwd = dir }, function(out)
					local data = order(out.stdout)

					vim.schedule(function()
						symbol_picker({ data = data })
					end)
				end)
			end,
			desc = "[C]scope [T]ags",
		},
	},
	opts = {
		disable_maps = true,
		skip_input_prompt = true,
		cscope = {
			picker = "telescope",
		},
	},
}
