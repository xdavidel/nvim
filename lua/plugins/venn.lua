return {
	"jbyuki/venn.nvim",
	keys = {
		{
			"<leader>ta",
			function()
				if vim.inspect(vim.b.venn_enabled) == "nil" then
					vim.b.venn_enabled = true
					vim.opt_local.ve = "all"
					-- draw a line on HJKL keystokes
					vim.api.nvim_buf_set_keymap(0, "n", "J", "<C-v>j:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "K", "<C-v>k:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "L", "<C-v>l:VBox<CR>", { noremap = true })
					vim.api.nvim_buf_set_keymap(0, "n", "H", "<C-v>h:VBox<CR>", { noremap = true })
					-- draw a box by pressing "f" with visual selection
					vim.api.nvim_buf_set_keymap(0, "v", "f", ":VBox<CR>", { noremap = true })
				else
					vim.opt_local.ve = nil
					vim.cmd([[mapclear <buffer>]])
					vim.b.venn_enabled = nil
				end
			end,
			desc = "[T]oggle [A]scii",
		},
	},
}
