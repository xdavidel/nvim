return {
	"tristone13th/lspmark.nvim",
	keys = {
		{
			"<leader>mt",
			function()
				require("lspmark.bookmarks").toggle_bookmark({ with_comment = true })
			end,
			desc = "[M]ark [T]oggle",
		},
		{
			"<leader>me",
			function()
				require("lspmark.bookmarks").modify_comment()
			end,
			desc = "[M]ark [E]dit",
		},
		{
			"<leader>ml",
			function()
				require("telescope").extensions.lspmark.lspmark()
			end,
			desc = "[M]ark [L]ist",
		},
	},
	config = function()
		local ok, telescope = pcall(require, "telescope")
		if not ok then
			return
		end

		require("lspmark").setup()
		telescope.load_extension("lspmark")
		require("lspmark.bookmarks").load_bookmarks()
		vim.api.nvim_create_autocmd({ "DirChanged" }, {
			callback = function()
				require("lspmark.bookmarks").load_bookmarks(nil)
			end,
			pattern = { "*" },
		})
	end,
}
