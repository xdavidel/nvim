return {
	"nvim-orgmode/orgmode",
	ft = { "org" },
	keys = {
		{
			"<leader>oa",
			function()
				local has_noice, noice = pcall(require, "noice")
				if has_noice then
					noice.cmd("disable")
				end

				require("orgmode").action("agenda.prompt")

				if has_noice then
					noice.cmd("enable")
				end
			end,
			desc = "[O]rg [A]genda",
		},
		{
			"<leader>oc",
			function()
				require("orgmode").action("capture.prompt")
			end,
			desc = "[O]rg [C]apture",
		},
	},
	config = function()
		local org_dir = "~/Documents/Org"
		require("orgmode").setup({
			org_agenda_files = {
				org_dir .. "/agenda/*",
			},
			org_default_notes_file = org_dir .. "/refile.org",
			org_todo_keywords = {
				"TODO(t)",
				"BUG(b)",
				"FIX(f)",
				"WAITING(w)",
				"REVIEW(r)",
				"|",
				"DONE(d)",
				"COMPLETED(c)",
				"CANCEL(x)",
			},
			org_todo_keyword_faces = {
				TODO = ":background cyan :foreground black :weight bold",
				BUG = ":background red :weight bold",
				FIX = ":background yellow :foreground black :weight bold",
				WAITING = ":background blue :weight bold",
				REVIEW = ":background orange :foreground black :weight bold",
				DONE = ":background green :weight bold",
				COMPLETED = ":background green :weight bold",
				CANCEL = ":background gray :weight bold",
			},
			org_capture_templates = {
				t = {
					description = "Task",
					template = "* TODO %?\n %U\n %a\n",
					target = org_dir .. "/agenda/tasks.org",
				},
				b = {
					description = "Bug",
					template = "* BUG %?\n %U\n %a\n",
					target = org_dir .. "/agenda/bugs.org",
				},
			},
			mappings = {
				global = {
					org_agenda = false,
					org_capture = false,
				},
			},
		})

		local has_cmp, cmp = pcall(require, "cmp")
		if has_cmp then
			cmp.setup({
				sources = {
					{ name = "orgmode" },
				},
			})
		end
	end,
}
