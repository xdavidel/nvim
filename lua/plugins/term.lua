local function toggleterm_float()
	require("toggleterm").toggle_command("direction=float")
end

local function toggleterm_bottom()
	require("toggleterm").toggle_command("direction=horizontal")
end

return {
	"akinsho/toggleterm.nvim",
	cmd = { "ToggleTerm" },
	keys = {
		{
			"<C-@>",
			toggleterm_bottom,
			desc = "Toggle horizontal terminal",
			mode = { "n", "i", "t" },
		},
		{
			[[<C-`>]],
			toggleterm_float,
			desc = "Toggle floating terminal",
			mode = { "n", "i", "t" },
		},
		{
			[[<C-\>]],
			toggleterm_float,
			desc = "Toggle floating terminal",
			mode = { "n", "i", "t" },
		},
		{
			"<leader>gl",
			function()
				local Terminal = require("toggleterm.terminal").Terminal
				local lazygit = Terminal:new({
					cmd = "lazygit",
					hidden = true,
					direction = "float",
					float_opts = {
						border = "none",
						width = 100000,
						height = 100000,
					},
					on_open = function(_)
						vim.cmd("startinsert!")
					end,
					on_close = function(_) end,
					count = 99,
				})
				lazygit:toggle()
			end,
			desc = "[G]it [L]azygit",
		},
	},
	config = function()
		require("toggleterm").setup({
			-- open_mapping = [[<C-Space>]],
			size = function(term)
				if term.direction == "horizontal" then
					return vim.o.lines * 0.8
				elseif term.direction == "vertical" then
					return vim.o.columns * 0.4
				end
			end,
		})
	end,
}
