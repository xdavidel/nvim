return {
	"lewis6991/gitsigns.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	event = "BufRead",
	cmd = "Gitsigns",
	keys = {
		{
			"<leader>gB",
			function()
				require("gitsigns").blame_line()
			end,
			desc = "[G]it [B]lame",
		},
	},
	opts = {
		signcolumn = true,
		numhl = false,
		linehl = false,
		word_diff = false,
		diff_opts = {
			internal = false, -- fix issue with CRLF
		},
	},
}
