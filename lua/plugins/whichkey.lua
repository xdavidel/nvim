return {
	"folke/which-key.nvim",
	event = { "UIEnter" },
	init = function()
		vim.o.timeout = true
		vim.o.timeoutlen = 300
	end,
	config = function()
		local wk = require("which-key")
		wk.setup({
			icons = {
				breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
				separator = "➜", -- symbol used between a key and it's label
				group = "+", -- symbol prepended to a group
			},
		})

		wk.add({
			{ "<leader>b", group = "Buffer" }, -- group
			{ "<leader>d", group = "Debug" }, -- group
			{ "<leader>f", group = "File" }, -- group
			{ "<leader>g", group = "Git" }, -- group
			{ "<leader>n", group = "Note" }, -- group
			{ "<leader>s", group = "Search" }, -- group
			{ "<leader>t", group = "Toggle" }, -- group
		})
	end,
}
