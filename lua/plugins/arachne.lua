local note_dir = vim.fn.expand("~/Documents/notes")

return {
	"xdavidel/arachne.nvim",
	dependencies = {
		"nvim-telescope/telescope.nvim",
		"nvim-lua/plenary.nvim",
	},
	config = function()
		require("arachne").setup({
			notes_directory = note_dir,
			file_extension = ".md",
		})
	end,
	init = function()
		local function find_notes()
			local ok, telescope_builtin = pcall(require, "telescope.builtin")
			if not ok then
				return
			end
			telescope_builtin.find_files({
				prompt_title = "<notes::files>",
				cwd = vim.fn.expand(note_dir),
			})
		end

		local function grep_notes()
			local ok, telescope_builtin = pcall(require, "telescope.builtin")
			if not ok then
				return
			end
			telescope_builtin.live_grep({
				prompt_title = "<notes::grep>",
				cwd = vim.fn.expand(note_dir),
			})
		end

		vim.keymap.set("n", "<leader>nn", function()
			require("arachne").new()
		end, { desc = "[N]ote [N]ew" })

		vim.keymap.set("n", "<leader>nN", function()
			vim.ui.input({ prompt = "File extension: " }, function(input)
				require("arachne").new(input)
			end)
		end, { desc = "[N]ote [N]ew typed" })

		vim.keymap.set("n", "<leader>nr", function()
			require("arachne").rename()
		end, { desc = "[N]ote [R]ename" })
		vim.keymap.set("n", "<leader>nf", find_notes, { desc = "[N]ote [F]ind" })
		vim.keymap.set("n", "<leader>ng", grep_notes, { desc = "[N]ote [G]rep" })
	end,
}
