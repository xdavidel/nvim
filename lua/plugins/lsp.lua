return {
	"neovim/nvim-lspconfig",
	event = { "BufRead", "BufNewFile" },
	dependencies = {
		"saghen/blink.cmp",
		{
			"williamboman/mason.nvim",
			enabled = not vim.g.nixenv,
		},
		{
			"williamboman/mason-lspconfig.nvim",
			enabled = not vim.g.nixenv,
		},
		{
			"folke/lazydev.nvim",
			ft = "lua", -- only load on lua files
			opts = {
				library = {
					-- See the configuration section for more details
					-- Load luvit types when the `vim.uv` word is found
					{ path = "${3rd}/luv/library", words = { "vim%.uv" } },
				},
			},
		},
		{
			"https://git.sr.ht/~whynothugo/lsp_lines.nvim",
		},
	},
	config = function()
		local lsp_keymaps = function(bufnr)
			vim.keymap.set(
				{ "n", "v" },
				"<leader>ca",
				vim.lsp.buf.code_action,
				{ desc = "[C]ode [A]ction", buffer = bufnr }
			)
			vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { desc = "[G]o to [D]eclaration", buffer = bufnr })
			vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, { desc = "Lsp Signature Help", buffer = bufnr })
			vim.keymap.set("n", "<leader>lr", vim.lsp.buf.rename, { desc = "[L]sp [R]ename", buffer = bufnr })
			vim.keymap.set("n", "<C-LeftMouse>", vim.lsp.buf.definition, { desc = "definition", buffer = bufnr })
			vim.keymap.set(
				"n",
				"gr",
				"<cmd>Telescope lsp_references<CR>",
				{ desc = "[G]o to [R]eferences", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"gd",
				"<cmd>Telescope lsp_definitions<CR>",
				{ desc = "[G]o to [D]efinition", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"gs",
				"<cmd>Telescope lsp_workspace_symbols<cr>",
				{ desc = "[G]o to [S]ymbols", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"<C-t>",
				"<cmd>Telescope lsp_document_symbols<cr>",
				{ desc = "[G]o to local [S]ymbols", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"gI",
				"<cmd>Telescope lsp_implementations<CR>",
				{ desc = "[G]o to [I]mplementation", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"<leader>lt",
				"<cmd>Telescope lsp_type_definitions<CR>",
				{ desc = "[L]sp [T]ypes", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"<leader>lfi",
				"<cmd>Telescope lsp_incoming_calls<CR>",
				{ desc = "[L]sp [F]unction [I]ncoming", buffer = bufnr }
			)
			vim.keymap.set(
				"n",
				"<leader>lfo",
				"<cmd>Telescope lsp_outgoing_calls<CR>",
				{ desc = "[L]sp [F]unction [O]utgoing", buffer = bufnr }
			)

			vim.keymap.set("n", "<leader>th", function()
				local ih = vim.lsp.inlay_hint
				ih.enable(not ih.is_enabled({}))
				vim.notify(string.format("inlay_hint %s", ih.is_enabled({})))
			end, { desc = "[T]oggle inlay [H]ints", buffer = bufnr })

			require("lsp_lines").setup()
			vim.diagnostic.config({ virtual_lines = false }) -- don't show by default

			vim.keymap.set("n", "<leader>tl", require("lsp_lines").toggle, { desc = "[T]oggle lsp [L]ines" })
		end

		local lsp_highlight_document = function(client, bufnr)
			if client.server_capabilities.documentHighlightProvider then
				vim.api.nvim_create_augroup("lsp_document_highlight", { clear = true })
				vim.api.nvim_clear_autocmds({ buffer = bufnr, group = "lsp_document_highlight" })
				vim.api.nvim_create_autocmd("CursorHold", {
					callback = vim.lsp.buf.document_highlight,
					buffer = bufnr,
					group = "lsp_document_highlight",
					desc = "Document Highlight",
				})
				vim.api.nvim_create_autocmd("CursorMoved", {
					callback = vim.lsp.buf.clear_references,
					buffer = bufnr,
					group = "lsp_document_highlight",
					desc = "Clear All the References",
				})
			end
		end

		local on_attach = function(client, bufnr)
			lsp_keymaps(bufnr)
			lsp_highlight_document(client, bufnr)
		end

		-- nvim-cmp supports additional completion capabilities
		local capabilities
		local has_blink, blink = pcall(require, "blink.cmp")
		if has_blink then
			capabilities = blink.get_lsp_capabilities()
		else
			capabilities = vim.lsp.protocol.make_client_capabilities()
		end

		local function configure_server(server_name)
			local opts = {
				on_attach = on_attach,
				capabilities = capabilities,
			}

			local has_custom_config, custom_config = pcall(require, "lsp/settings/" .. server_name)
			if has_custom_config then
				opts = vim.tbl_deep_extend("force", opts, custom_config)
			end

			require("lspconfig")[server_name].setup(opts)
		end

		if not vim.g.nixenv then
			require("mason").setup()
			require("mason-lspconfig").setup()
			require("mason-lspconfig").setup_handlers({
				-- The first entry (without a key) will be the default handler
				-- and will be called for each installed server that doesn't have
				-- a dedicated handler.
				function(server_name) -- default handler (optional)
					configure_server(server_name)
				end,
			})
		else
			-- setup from runtime
			local built_in_servers = { "clangd", "rust_analyzer", "zls", "pyright", "nil_ls", "lua_ls" }
			for _, server_name in ipairs(built_in_servers) do
				configure_server(server_name)
			end
		end

		-- add experimental cscope_lsp
		local function start_cscope_lsp()
			local lsp_server = "cscope_lsp"
			if vim.fn.executable(lsp_server) ~= 1 then
				return
			end
			local root_files = {
				"cscope.out",
				"cscope.files",
				"cscope.in.out",
				"cscope.out.in",
				"cscope.out.po",
				"cscope.po.out",
			}
			local paths = vim.fs.find(root_files, { stop = vim.env.HOME })
			local root_dir = vim.fs.dirname(paths[1])

			if root_dir then
				vim.lsp.start({
					name = lsp_server,
					cmd = { lsp_server },
					root_dir = root_dir,
					offset_encoding = "utf-8",
					filetypes = { "c", "h", "cpp", "hpp" },
				})
			end
		end

		vim.api.nvim_create_autocmd("FileType", {
			pattern = { "c", "h", "cpp", "hpp" },
			desc = "Start cscope_lsp",
			callback = start_cscope_lsp,
		})

		-- Globals bindings
		vim.keymap.set("n", "<leader>li", "<cmd>LspInfo<CR>", { desc = "[L]sp [I]nfo" })
		vim.keymap.set("n", "<leader>lR", "<cmd>LspRestart<CR>", { desc = "[L]sp [R]estart" })
	end,
}
