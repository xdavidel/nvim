return {
	{
		"stevearc/oil.nvim",
		cmd = "Oil",
		config = true,
		opts = {
			default_file_explorer = true,
			columns = {
				"icon",
				"size",
			},
		},
	},
}
