return {
	"ur4ltz/surround.nvim",
	event = { "BufRead", "BufNewFile" },
	config = function()
		require("surround").setup({
			mappings_style = "surround",
		})
	end,
}
