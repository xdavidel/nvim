local tab_spaces = 2
local lo = vim.opt_local

lo.tabstop = tab_spaces
lo.softtabstop = tab_spaces
lo.shiftwidth = tab_spaces
